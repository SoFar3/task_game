package com.epam.model;

public enum Sign {

    FIELD_SIGN("+"),
    ATTACKED_FIELD_SIGN("+"),
    SHIP_SIGN("+"),
    DESTROYED_SHIP_SIGN("+"),
    DESTROYED_DECK_SIGN("+");

    private String sign;

    Sign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return this.sign;
    }

}
