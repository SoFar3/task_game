package com.epam.model;

public class Ship {

    private Position start;
    private Position end;

    private int amountOfDecks;

    public Ship(Position start, Position end, int amountOfDecks) {
        this.start = start;
        this.end = end;
        this.amountOfDecks = amountOfDecks;
    }

    public Position getStart() {
        return start;
    }

    public void setStart(Position start) {
        this.start = start;
    }

    public Position getEnd() {
        return end;
    }

    public void setEnd(Position end) {
        this.end = end;
    }

    public int getAmountOfDecks() {
        return amountOfDecks;
    }

    public void setAmountOfDecks(int amountOfDecks) {
        this.amountOfDecks = amountOfDecks;
    }

}
